const STATES = {
  UNOPENED: 0,
  OPENING: 1,
  OPENING_OUT: 2,
  FANNING_OUT_CARDS: 3,
  FANNING_DONE_CARDS: 4,
};
const OPEN_DIST_LIMIT = 100;
const OPEN_OUT_DIST_LIMIT = 400;
const FAN_OUT_DIST_LIMIT = 100;
const CARD_FLIP_LIMIT = 30;
const OPEN_SPEED = 5;
const OPEN_OUT_SPEED = 30;
const FANNING_SPEED = 2;

const ID_IGNORE = 10;
const ID_TOP_BOX = 200;
const ID_CARD_RANGE = 1000; // 1000-2000

let state = STATES.UNOPENED;
let openDist = 0;
let openOutDist = 0;
let fanningOutDist = 0;
let flippedId = {};
let cardFlipDist = {};

class Card {
  constructor(row, col, image) {
    this.row = row;
    this.col = col;
    this.image = image;
  }
}

function preload() {
  BOX_WIDTH = 1083;
  BOX_HEIGHT = 1457;
  BOX_DEPTH = 345;
  FRONT_IMG = loadImage("images/front.png");
  LEFT_IMG = loadImage("images/left.png");
  TOP_IMG = loadImage("images/top.png");
  RIGHT_IMG = loadImage("images/right.png");
  BOTTOM_IMG = loadImage("images/bottom.png");
  BACK_IMG = loadImage("images/back.png");
  BASE_TOP_IMG = loadImage("images/base_top.png");
  BASE_SIDE_IMG = loadImage("images/base_side.png");
  DLNK_CARDS = [];
  for (let dynasty = 1; dynasty <= 2; dynasty++) {
    for (let role = 1; role <= 4; role++) {
      let fileName = `images/DLNK_${dynasty}.${role}.jpg`;
      let image = loadImage(fileName);
      let card = new Card(dynasty, role, image);
      DLNK_CARDS.push(card);
    }
  }
}

function setup() {
  mCreateCanvas(windowWidth, windowHeight, WEBGL);
  // Make sure the box always fit the screen.
  SCALE_FACTOR =
    windowHeight / 2 / Math.max(Math.max(BOX_WIDTH, BOX_HEIGHT), BOX_DEPTH);
  state = STATES.UNOPENED;
}

function updateState() {
  switch (state) {
    case STATES.UNOPENED:
      break;
    case STATES.OPENING:
      if (openDist > OPEN_DIST_LIMIT) {
        state = STATES.OPENING_OUT;
      } else {
        openDist += OPEN_SPEED;
      }
      break;
    case STATES.OPENING_OUT:
      if (openOutDist > OPEN_OUT_DIST_LIMIT) {
        //state = STATES.FANNING_OUT_CARDS;
      } else {
        openOutDist += OPEN_OUT_SPEED;
      }
      break;
    case STATES.FANNING_OUT_CARDS:
      if (fanningOutDist > FAN_OUT_DIST_LIMIT) {
        state = STATES.FANNING_DONE_CARDS;
      } else {
        fanningOutDist += FANNING_SPEED;
      }
      break;
  }
}

function drawBackHalf(w, h, d) {
  mPush();
  mTexture(BACK_IMG);
  mRotateY(180);
  mTranslate(-w, 0, d);
  mTranslate(0, 0, fanningOutDist);
  mQuad(ID_IGNORE, 0, 0, w, 0, w, h, 0, h);
  mPop();

  mPush();
  mTexture(BASE_SIDE_IMG);
  mTranslate(0, 0, -d);
  mTranslate(0, 0, -fanningOutDist);
  mRotateY(-90);
  mQuad(ID_IGNORE, 0, 0, d, 0, d, h, 0, h);
  mPop();

  mPush();
  mTexture(BASE_TOP_IMG);
  mTranslate(0, 0, -d);
  mTranslate(0, 0, -fanningOutDist);
  mRotateX(90);
  mQuad(ID_IGNORE, 0, 0, w, 0, w, d, 0, d);
  mPop();

  mPush();
  mTexture(BASE_SIDE_IMG);
  mTranslate(w, 0, 0);
  mTranslate(0, 0, -fanningOutDist);
  mRotateY(90);
  mQuad(ID_IGNORE, 0, 0, d, 0, d, h, 0, h);
  mPop();

  mPush();
  mTexture(BASE_TOP_IMG);
  mTranslate(0, h, 0);
  mTranslate(0, 0, -fanningOutDist);
  mRotateX(-90);
  mQuad(ID_IGNORE, 0, 0, w, 0, w, d, 0, d);
  mPop();
}

function drawFrontHalf(w, h, d) {
  mPush();
  mTexture(FRONT_IMG);
  mTranslate(0, 0, openDist);
  mTranslate(0, -openOutDist, 0);
  mQuad(ID_TOP_BOX, 0, 0, w, 0, w, h, 0, h);
  mPop();

  mPush();
  mTexture(LEFT_IMG);
  mTranslate(-1, 0, -d);
  mTranslate(0, 0, openDist);
  mTranslate(0, -openOutDist, 0);
  mRotateY(-90);
  mQuad(ID_IGNORE, 0, 0, d, 0, d, h, 0, h);
  mPop();

  mPush();
  mTexture(TOP_IMG);
  mTranslate(0, -1, -d);
  mTranslate(0, 0, openDist);
  mTranslate(0, -openOutDist, 0);
  mRotateX(90);
  mQuad(ID_IGNORE, -1, -1, w + 1, -1, w + 1, d + 1, -1, d + 1);
  mPop();

  mPush();
  mTexture(RIGHT_IMG);
  mTranslate(w + 1, 0, 0);
  mTranslate(0, 0, openDist);
  mTranslate(0, -openOutDist, 0);
  mRotateY(90);
  mQuad(ID_IGNORE, 0, 0, d, 0, d, h, 0, h);
  mPop();

  mPush();
  mTexture(BOTTOM_IMG);
  mTranslate(0, h + 1, 0);
  mTranslate(0, 0, openDist);
  mTranslate(0, -openOutDist, 0);
  mRotateX(-90);
  mQuad(ID_IGNORE, 0, 0, w, 0, w, d, 0, d);
  mPop();
}

function drawCards(w, h, d) {
  let l = DLNK_CARDS.length;
  DLNK_CARDS.forEach((card, i) => {
    mPush();
    mTexture(card.image);
    let spacingX = w * 1.1;
    let spacingY = h * 1.1;
    let offsetX = 0;
    let offsetY = 0;
    let xD = (card.col - 2.5) * w;
    let yD = card.row * h;
    let zD = (d / l) * (l - i - 1) - d;
    let ratio = Math.easeInOutQuad(
      fanningOutDist / FAN_OUT_DIST_LIMIT,
      0,
      1,
      1
    );
    let x = xD * ratio;
    let y = yD * ratio;
    let z = zD + d * ratio;
    let cardId = ID_CARD_RANGE + i;

    let halfWidth = Math.round(w / 2);
    mTranslate(x + halfWidth, y, z);
    if (flippedId[cardId]) {
      if (cardFlipDist[cardId] < CARD_FLIP_LIMIT) {
        cardFlipDist[cardId]++;
      }
      mRotateY(cardFlipDist[cardId] * 6);
    }
    mQuad(
      cardId,
      -halfWidth,
      1,
      halfWidth,
      1,
      halfWidth,
      h - 1,
      -halfWidth,
      h - 1
    );
    mPop();
  });
}

function drawBox() {
  updateState();

  let w = BOX_WIDTH * SCALE_FACTOR;
  let h = BOX_HEIGHT * SCALE_FACTOR;
  let d = BOX_DEPTH * SCALE_FACTOR;

  // Center the box.
  mTranslate(-w / 2, -h / 2);

  drawBackHalf(w, h, d);
  drawFrontHalf(w, h, d);
  drawCards(w, h, d);
}

function rotateByMouse() {
  mAngleMode(DEGREES);
  mRotateX((mouseY - windowHeight / 2) / 10);
  mRotateY((windowWidth / 2 - mouseX) / 10);
  let ratio = Math.easeInOutQuad(fanningOutDist / FAN_OUT_DIST_LIMIT, 0, 1, 1);
  mTranslate(0, -((2 / 3) * windowHeight) * ratio, 0);
}

function draw() {
  mBackground(50);
  mPage.reset();

  rotateByMouse();

  drawBox();
}

function mouseClicked() {
  if (state === STATES.UNOPENED) {
    state = STATES.OPENING;
  }
  if (state === STATES.OPENING_OUT) {
    state = STATES.FANNING_OUT_CARDS;
  }
  let clickedId = objectAtMouse();
  if (state == STATES.FANNING_DONE_CARDS && clickedId >= ID_CARD_RANGE) {
    flippedId[clickedId] = !flippedId[clickedId];
    cardFlipDist[clickedId] = 0;
  }
}

Math.easeInOutQuad = function (t, b, c, d) {
  t /= d / 2;
  if (t < 1) return (c / 2) * t * t + b;
  t--;
  return (-c / 2) * (t * (t - 2) - 1) + b;
};
